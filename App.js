import React from "react";
import { StyleSheet, View, Button, Modal } from "react-native";

export default class App extends React.Component {
  state = {
    visible: false
  };
  handlePress = () => {
    this.setState({ visible: !this.state.visible });
  };
  render() {
    return (
      <View style={[styles.container, styles.cyan]}>
        <Modal animationType="slide" visible={this.state.visible}>
          <View style={[styles.container, styles.gray]}>
            <Button title="Cerrar Modal" onPress={this.handlePress}/>
          </View>
        </Modal>
        <Button title="Abrir Modal" onPress={this.handlePress}/>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    display: "flex",
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  cyan: {
    backgroundColor: "cyan"
  },
   gray: {
     backgroundColor: "gray"
   }
});